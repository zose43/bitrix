<?php

if ($_GET["page"] && !$_GET['reload'])
    require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
else
    require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");


$APPLICATION->IncludeComponent("vm:test", "", ['pagination' => 9]);


if (!$_GET["page"])
    require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php");
