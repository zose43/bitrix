<?php
use \Bitrix\Main\Page\Asset;

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

class VMTest extends CBitrixComponent
{
    private function getUsers()
    {
        $connection = \Bitrix\Main\Application::getConnection();
        $sql = "
        SELECT LOGIN as login, ACTIVE as status, NAME as name, EMAIL as mail
        FROM b_user
        WHERE ACTIVE = 'Y'
        ORDER BY LOGIN
        ";
        $aSql = 'SELECT count(*) as count FROM b_user';

        $res = $connection->query($sql);
        $aRes = $connection->query($aSql)->fetchAll();

        $this->arResult['all'] = $aRes[0]['count'];

        if (!empty($res))
            return $res;

        return false;
    }

    private function makeArr()
    {
        $data = $this->getUsers();
        $usersArr = [];

        if (!$data)
            return false;

        foreach ($data as $item)
        {
            if (!$item['name'])
                $item['name'] = 'Имя не заполнено';
            if (!$item['mail'])
                $item['mail'] = 'Почта не заполнена';

            $usersArr[] = $item;
        }

        return $usersArr;
    }

    private function createPagination(int $page=1, bool $reload=false)
    {
        if (!intval($page))
            return false;

        $count = $this->arResult['pagCount'];
        $offset = $count * $page;
        $data = $this->makeArr();
        $toSend = [];
        $i = ($page - 1) * 10;

        if ($reload)
        {
            $count = ($page - 1)  * 10 - 1;
            $offset = $count;
            $i = $offset;
        }

        for ($offset;$count >= 0; $count--)
        {
            if (!$data[$offset])
                return false;

            $i++;
            if ($page > 1 && !$reload)
            {
                $login = $data[$offset]['login'];
                $mail = $data[$offset]['mail'];
                $status = $data[$offset]['status'];
                $name = $data[$offset]['name'];
                $htmlEl = "<tr><th scope=\"row\">$i</th><td>$login</td><td>$status</td><td>$name</td><td>$mail</td></tr>";
                $toSend[] = $htmlEl;

            } else
            {
                $toSend[] = $data[$offset];
            }

            $offset--;
        }

        if ($page == 1 || $reload)
            $this->arResult['items'] = $toSend;
        else
            $this->sendData($toSend);
    }

    private function sendData(array $data)
    {
        if (!count($data))
            return false;

        $res = json_encode($data, JSON_UNESCAPED_UNICODE);
        echo $res;
    }

    public function onPrepareComponentParams($params)
    {
        $this->arResult['pagCount'] = $params['pagination'];
    }

    public function executeComponent()
    {
        Asset::getInstance()->addCss('/vendor/twbs/dist/css/bootstrap.min.css');

        if ($_GET['page'] && $_GET['reload'])
        {
            $this->createPagination($_GET['page'], true);
            $this->includeComponentTemplate();

        }
        elseif ($_GET['page'] && !$_GET['reload'])
            $this->createPagination(intval($_GET['page']));

        else
        {
            $this->createPagination();
            $this->includeComponentTemplate();

        }
    }
}