<?php
$i = 0;
?>
<div class="container">
    <div class="row">
        <div class="col">
            <div class="title h2 text-center mt-3">Статистика пользователей</div>
        </div>
        <table class="table mt-2 table-hover">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Логин</th>
                <th scope="col">Статус</th>
                <th scope="col">Имя</th>
                <th scope="col">Почта</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($arResult['items'] as $user): $i++; ?>
                <tr>
                    <th scope="row"><?=$i ?></th>
                    <td><?=$user['login'] ?></td>
                    <td><?=$user['status'] ?></td>
                    <td><?=$user['name'] ?></td>
                    <td><?=$user['mail'] ?></td>
                </tr>
            <?php endforeach;?>
            </tbody>
        </table>
        <div class="all mt-3">Всего зарегистрировано <?=$arResult['all']?> пользователей</div>
        <button type="button" class="btn btn-primary btn-lg btn-block my-5 seeMore">Показать еще</button>
    </div>
</div>
