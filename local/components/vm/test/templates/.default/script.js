$(document).ready(function () {
    const seeMore = $('.seeMore');
    const params = (new URL(document.location)).searchParams;

    window.page = params.get('page') ? params.get('page') : 2;

    $(seeMore).on('click', function () {
        window.history.pushState(null, null,'?page=' + page);
        sendAjax(window.page);
    })

    $(document).on('keydown', function (e)
    {
        if (e.keyCode == 116 || event.ctrlKey && e.which == 82)
        {
            window.history.pushState(null, null, '?reload=true&page=' + page);
        }
    });
});
function sendAjax(page)
{
    $.ajax({
        url: document.location,
        method: "GET",
        success: function(data)
        {
            if (!data)
                return false;

            data = JSON.parse(data);
            window.page++;

            for (item in data)
                createItems(data[item]);

        }
    });
}

function createItems(html)
{
    tBody = $('tbody')[0];
    $(tBody).append(html);
}
